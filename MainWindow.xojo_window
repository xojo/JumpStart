#tag Window
Begin Window MainWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   370
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   239484927
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   False
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "JumpStart"
   Visible         =   True
   Width           =   680
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Close()
		  SaveData
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Deactivate()
		  If App.IsAutoHide Then
		    // Automatically hide the window when it loses focus, such
		    // as when an app is launched
		    Self.Hide
		  End If
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  // Add AppControls to window, fitting as many as possible given the window size
		  Const kMargin = 20
		  
		  Dim isFull As Boolean
		  Dim ac As AppControl
		  Dim acLeft As Integer = kMargin
		  Dim acTop As Integer = kMargin
		  
		  Do
		    ac = New AppControl
		    
		    // The array property is used for saving button data.
		    // It would also be useful should you want to implement
		    // window resizing as you'll want to easily iterate through
		    // the controls to possibly reposition them or add more.
		    AppButtons.Append(ac)
		    ac.EmbedWithin(Self, acLeft, acTop)
		    
		    acLeft = acLeft + ac.Width + kMargin
		    If acLeft + ac.Width > Self.Width Then
		      // Move to next row
		      acLeft = kMargin
		      acTop = acTop + ac.Height + kMargin
		      
		      If acTop + ac.Height > Self.Height Then
		        // No more room in window
		        isFull = True
		      End If
		    End If
		    
		  Loop Until isFull
		  
		  LoadData
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub LoadData()
		  // Load app settings from JSON and set up the buttons
		  
		  Dim jumpStartFolder As FolderItem = SpecialFolder.Preferences.Child("JumpStart")
		  If Not jumpStartFolder.Exists Then
		    Return
		  End If
		  
		  Dim loadFile As FolderItem = jumpStartFolder.Child("JumpStart.json")
		  Dim inputStream As TextInputStream
		  inputStream = TextInputStream.Open(loadFile)
		  inputStream.Encoding = Encodings.UTF8
		  Dim json As String = inputStream.ReadAll
		  inputStream.Close
		  
		  Dim jsonArray As New JSONItem(json)
		  
		  For i As Integer = 0 To jsonArray.Count - 1
		    Dim jsonApp As JSONItem = jsonArray.Value(i)
		    AppButtons(i).Caption = jsonApp.Value("AppCaption")
		    
		    Dim appPath As String = jsonApp.Value("AppPath")
		    If appPath <> "" Then
		      AppButtons(i).AppFile = New FolderItem(appPath, FolderItem.PathTypeNative)
		    End If
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SaveData()
		  // Save app settings to JSON file
		  
		  // Create the JSON as an array for each button with JSON for the button
		  // Caption and app path.
		  Dim jsonArray As New JSONItem
		  Dim jsonApp As JSONItem
		  For i As Integer = 0 To AppButtons.Ubound
		    jsonApp = New JSONItem
		    If AppButtons(i).AppFile <> Nil Then
		      jsonApp.Value("AppCaption") = AppButtons(i).Caption
		      jsonApp.Value("AppPath") = AppButtons(i).AppFile.NativePath
		    Else
		      jsonApp.Value("AppCaption") = ""
		      jsonApp.Value("AppPath") = ""
		    End If
		    jsonArray.Append(jsonApp)
		  Next
		  
		  // Save json to text file
		  Dim json As String = jsonArray.ToString
		  
		  Dim jumpStartFolder As FolderItem = SpecialFolder.Preferences.Child("JumpStart")
		  If Not jumpStartFolder.Exists Then
		    jumpStartFolder.CreateAsFolder
		  End If
		  
		  Dim saveFile As FolderItem = jumpStartFolder.Child("JumpStart.json")
		  Dim output As TextOutputStream
		  output = TextOutputStream.Create(saveFile)
		  output.Write(json)
		  output.Close
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private AppButtons() As AppControl
	#tag EndProperty


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Deprecated"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
#tag EndViewBehavior
